<?xml version="1.0" encoding="UTF-8"?>

<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="Emojii" version="1.1" date="1/10/2019" >
		<VersionSettings gameVersion="1.3.0" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Author name="Sullemunk" email="" />
		<Description text="Just some emojii stuffs" />
		        <Dependencies>
		    <Dependency name="EATemplate_DefaultWindowSkin" />
            <Dependency name="EASystem_Utils" />
            <Dependency name="EASystem_WindowUtils" />
			 <Dependency name="EA_ChatWindow" />
			 <Dependency name="EA_ChatSystem" />
		 
			
		
		</Dependencies>
		<Files>
			<File name="Emojii.lua" />
			<File name="Emojii.xml" />		
		</Files>
		<OnInitialize>
			<CallFunction name="Emojii.OnInitialize" /> 
		</OnInitialize>
				<OnUpdate>
			<CallFunction name="Emojii.UpdateEmojiWindows" /> 
		</OnUpdate>
		
		<OnShutdown>
<!--			<CallFunction name="Emojii.OnShutdown" /> -->
    		</OnShutdown>
	</UiMod>
</ModuleFile>
