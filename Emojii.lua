
Emojii = {}

local tinsert = table.insert

Emojii.EmojiWindowList = {}
Emojii.EmojiList = {happy=31001,smile=31004,wink=31006,HeartEyes=31007,kiss=31008,toung=31012,flushed=31015,grin=31016,sad=31017,persevere=31021,sob=31022,joy=31023,cry=31024,think=31025,ColdSweat=31027,Sweat=31029,weary=31030,
				fearfull=31032,scream=31033,angry=31035,triumph=31036,confounded=31037,yum=31039,sleepy=31042,dizzy=31043,EvilSmile=31048,EvilSad=31049,Confused=31053,innocent=31056,smirk=31057,poop=31090,ThumbsUp=31106,ThumbsDown=31107,Ok=31108,Punch=31109,
				Fist=31110,peace=31111,wave=31112,PointUp=31115,PointDown=31116,PointRight=31117,PointLeft=31118,cheer=31119,pray=31120,clap=31122,FML=31204,TreeMan=31213}
Emojii.EmojiTimer = 5
Emojii.ActiveTab = 1
Emojii.SelectedMoji = 31001

function Emojii.OnInitialize()

	Emojii.SelfObjNum = GameData.Player.worldObjNum
	CreateWindow ("EmojiiChooseIconDialog", false)
	CreateWindow("EmojiiEmojii", true)
			
	RegisterEventHandler( SystemData.Events.CHAT_TEXT_ARRIVED, "Emojii.OnChatText")
			
	local texture, x, y = GetIconData (Emojii.SelectedMoji)
	DynamicImageSetTexture ("EmojiiEmojiiIconIcon", texture, 64, 64)

			
	ButtonSetText("EmojiiChooseIconDialogTab1",L"People")
	ButtonSetText("EmojiiChooseIconDialogTab2",L"Gesture")
	ButtonSetText("EmojiiChooseIconDialogTab3",L"Misc")

	ButtonSetPressedFlag( "EmojiiChooseIconDialogTab1",true)
	ButtonSetPressedFlag( "EmojiiChooseIconDialogTab2",false)
	ButtonSetPressedFlag( "EmojiiChooseIconDialogTab3",false)

	LayoutEditor.RegisterWindow( "EmojiiEmojii", L"EmojiiEmojii", L"EmojiiEmojii", false, false, false, nil )
	
Emojii.Emotelist = {[31001]="happy",[31004]="smile",[31006]="wink",[31007]="HeartEyes",[31008]="kiss",[31012]="toung",[31015]="flushed",[31016]="grin",[31017]="sad",[31021]="persevere",[31022]="sob",[31023]="joy",
				[31024]="cry",[31025]="think",[31027]="ColdSweat",[31029]="Sweat",[31030]="weary",[31032]="fearfull",[31033]="scream",[31035]="angry",[31036]="triumph",[31037]="confounded",
				[31039]="yum",[31042]="sleepy",[31043]="dizzy",[31048]="EvilSmile",[31049]="EvilSad",[31053]="Confused",[31056]="innocent",[31057]="smirk",[31090]="poop",[31106]="ThumbsUp",[31107]="ThumbsDown",[31108]="Ok",
				[31109]="Punch",[31110]="Fist",[31111]="peace",[31112]="wave",[31115]="PointUp",[31116]="PointDown",[31117]="PointRight",[31118]="PointLeft",[31119]="cheer",[31120]="pray",[31122]="clap",
				[31204]="FML",[31213]="TreeMan"}
end


function Emojii.OnShutdown()

end

function Emojii.UI_ChooseIconDialog_IsOpen ()
	return WindowGetShowing ("EmojiiChooseIconDialog")

end
function Emojii.UI_ChooseIconDialog_Hide ()
	WindowSetShowing ("EmojiiChooseIconDialog", false)
end


function Emojii.UI_ChooseIconDialog_Open (onOkCallback)
	
	if (not choose_icon_dlg.isInitialized)
	then
		LabelSetText ("EmojiiChooseIconDialogTitleBarText", L"Choose icon")		
		Emojii.UI_ChooseIconDialog_UpdateList ()		
		choose_icon_dlg.isInitialized = true
	end
	choose_icon_dlg.onOkCallback = onOkCallback	
	WindowSetShowing ("EmojiiChooseIconDialog", true)
end


function Emojii.UI_ChooseIconDialog_UpdateList ()

	Emojii.chooseIconDialogListData = {}
	Emojii.chooseIconDialogListIndexes = {}
	
	
	local minID = Emojii.ActiveTab
	local k = 1
	local id = 31000 + (100*(Emojii.ActiveTab-1))
	local max_id = 31000 + (100*Emojii.ActiveTab)
	--	local max_id = 31183	
	while (id < max_id)
	do
		local data = {}
		
		while (id < max_id and #data < 10)
		do
			local texture, x, y = GetIconData (id)
			
			if (texture and texture ~= "icon-00001" and texture ~= "icon-00002")
			then
				tinsert (data, id)
			end
			
			id = id + 1
		end
		
		tinsert (Emojii.chooseIconDialogListData, data)
		tinsert (Emojii.chooseIconDialogListIndexes, k)
		k = k + 1
	end
	
	ListBoxSetDisplayOrder ("EmojiiChooseIconDialogIcons", Emojii.chooseIconDialogListIndexes)
end


function Emojii.UI_ChooseIconDialog_OnIconsPopulate ()

	if (EmojiiChooseIconDialogIcons.PopulatorIndices == nil) then return end
	
	local row_window_name
	local data
	
	for k, v in ipairs (EmojiiChooseIconDialogIcons.PopulatorIndices)
	do
		row_window_name = "EmojiiChooseIconDialogIconsRow"..k
		data = Emojii.chooseIconDialogListData[v]
		
		for i = 1, 10
		do
			local id = data[i]
			
			if (id)
			then
				WindowSetShowing (row_window_name.."Icon"..i, true)
				
				local texture, x, y = GetIconData (id)
				DynamicImageSetTexture (row_window_name.."Icon"..i.."Icon", texture, 64, 64)
			else
				WindowSetShowing (row_window_name.."Icon"..i, false)
			end
		end
	end
end


function Emojii.UI_ChooseIconDialog_OnListRowLButtonUp ()

	local data_index = ListBoxGetDataIndex ("EmojiiChooseIconDialogIcons", WindowGetId (SystemData.ActiveWindow.name))
	local icon_index = tonumber (SystemData.ActiveWindow.name:match ("EmojiiChooseIconDialogIconsRow%d+Icon(%d+)"))	
	local IconToNumber = Emojii.Emotelist[tonumber(Emojii.chooseIconDialogListData[data_index][icon_index])]

	if (Emojii.EmojiWindowList[tostring(Emojii.SelfObjNum)] == nil) then
		SendChatText(L"/emote "..towstring(IconToNumber), L"")	
	end
		
	local texture, x, y = GetIconData (tonumber(L"31"..wstring.sub( Emojii.chooseIconDialogListData[data_index][icon_index],3)))
	DynamicImageSetTexture ("EmojiiEmojiiIconIcon", texture, 64, 64)				
	Emojii.SelectedMoji = tonumber(L"31"..wstring.sub( Emojii.chooseIconDialogListData[data_index][icon_index],3))			
	Emojii.UI_ChooseIconDialog_Hide ()
end

function Emojii.UI_ChooseIconDialog_OnListRowRButtonUp ()
	local data_index = ListBoxGetDataIndex ("EmojiiChooseIconDialogIcons", WindowGetId (SystemData.ActiveWindow.name))
	local icon_index = tonumber (SystemData.ActiveWindow.name:match ("EmojiiChooseIconDialogIconsRow%d+Icon(%d+)"))
	local IconToNumber = Emojii.Emotelist[tonumber(Emojii.chooseIconDialogListData[data_index][icon_index])]

	if (Emojii.EmojiWindowList[tostring(Emojii.SelfObjNum)] == nil) then	
		SendChatText(L"/emote "..towstring(IconToNumber), L"")	
	end
	local texture, x, y = GetIconData (tonumber(L"31"..wstring.sub( Emojii.chooseIconDialogListData[data_index][icon_index],3)))
	DynamicImageSetTexture ("EmojiiEmojiiIconIcon", texture, 64, 64)				
	Emojii.SelectedMoji = tonumber(L"31"..wstring.sub( Emojii.chooseIconDialogListData[data_index][icon_index],3))			
end



function Emojii.ToggleEmojii()
	Emojii.UI_ChooseIconDialog_UpdateList ()
	WindowSetShowing ("EmojiiChooseIconDialog", not WindowGetShowing("EmojiiChooseIconDialog"))
end


function Emojii.RunEmojii()
	if (Emojii.EmojiWindowList[tostring(Emojii.SelfObjNum)] == nil) then
		SendChatText(L"/emote "..towstring(Emojii.Emotelist[Emojii.SelectedMoji]), L"")	
	end
end

function Emojii.OnChatText()
	if GameData.ChatData.type == SystemData.ChatLogFilters.EMOTE then
		Emojii.AddChatText( GameData.ChatData.objectId, GameData.ChatData.text,GameData.ChatData.name,GameData.ChatData.type )    
	end
end

function Emojii.Emoji(worldObjNum,text)
	local WNumber = tostring(worldObjNum)
	if DoesWindowExist("EmojiWindow"..WNumber) == false then
	CreateWindowFromTemplate("EmojiWindow"..WNumber, "EmojiiWindowTemplate", "Root" )
	Emojii.EmojiWindowList[tostring(worldObjNum)] = Emojii.EmojiTimer
	AttachWindowToWorldObject("EmojiWindow"..WNumber, tonumber(worldObjNum))
	MoveWindowToWorldObject( "EmojiWindow"..WNumber, tonumber(worldObjNum), 0.999 )


		for k,v in pairs(Emojii.EmojiList) do
			if string.match(tostring(text),tostring(k)) then
				local txtr, x, y = GetIconData (tonumber(v))			
				DynamicImageSetTexture("EmojiWindow"..WNumber.."Icon",txtr, 64, 64)
				LabelSetText("EmojiWindow"..WNumber.."Name",towstring(k))

			end
		end
		WindowSetTintColor("EmojiWindow"..WNumber.."Flash",255,155,0)
		AnimatedImageStartAnimation ("EmojiWindow"..WNumber.."Flash", 0, false, true, 0)
		WindowStartAlphaAnimation("EmojiWindow"..WNumber, Window.AnimationType.SINGLE_NO_RESET, 0, 1, 0.5, false, 0, 0)

	end
end

function Emojii.UpdateEmojiWindows( timePassed )
	if Emojii.EmojiWindowList then
		for k,v in pairs(Emojii.EmojiWindowList) do
			if Emojii.EmojiWindowList[tostring(k)] > 0 then
					Emojii.EmojiWindowList[tostring(k)] = Emojii.EmojiWindowList[tostring(k)] - timePassed
					MoveWindowToWorldObject("EmojiWindow"..tostring(k), tonumber(k), 0.999 )
					ForceUpdateWorldObjectWindow( tonumber(k),"EmojiWindow"..tostring(k))  
				
			if Emojii.EmojiWindowList[k] > 1 and WindowGetAlpha("EmojiWindow"..tostring(k)) < 1 then
			WindowClearAnchors("EmojiWindow"..tostring(k).."Icon")
			WindowAddAnchor( "EmojiWindow"..tostring(k).."Icon", "center", "EmojiWindow"..tostring(k), "center", 0,-91+(Emojii.EmojiWindowList[k]*(Emojii.EmojiWindowList[k]*Emojii.EmojiWindowList[k])))
			end	
			
			
			if Emojii.EmojiWindowList[k] < 1 and WindowGetAlpha("EmojiWindow"..tostring(k)) == 1 then
				WindowStartAlphaAnimation("EmojiWindow"..tostring(k), Window.AnimationType.SINGLE_NO_RESET_HIDE, WindowGetAlpha("EmojiWindow"..tostring(k)), 0, 0.5, false, 0, 0)	
			end
						
			else
				DetachWindowFromWorldObject("EmojiWindow"..tostring(k),tonumber(k))
				Emojii.EmojiWindowList[tostring(k)] = nil
				DestroyWindow("EmojiWindow"..tostring(k))
			end
		end
	end
end

function Emojii.AddChatText( worldObjNum, text,name,Type )
	local worldObjNum = worldObjNum
	local text = text
	local Type = Type

    if ( worldObjNum == 0 ) then
        return
    end

	if Type == SystemData.ChatLogFilters.EMOTE then
		Emojii.Emoji(worldObjNum,text)
		return
	end
end

function Emojii.SelectTab()
	local TabNumber	= WindowGetId (SystemData.ActiveWindow.name)
	Emojii.ActiveTab = TabNumber
	Emojii.UI_ChooseIconDialog_UpdateList ()

	ButtonSetPressedFlag( "EmojiiChooseIconDialogTab1",1==TabNumber)
	ButtonSetPressedFlag( "EmojiiChooseIconDialogTab2",2==TabNumber)
	ButtonSetPressedFlag( "EmojiiChooseIconDialogTab3",3==TabNumber)
end

function Emojii.OnMouseOverStart()
	local WinParent = WindowGetParent(SystemData.MouseOverWindow.name)
	local WindowName = towstring(SystemData.MouseOverWindow.name)
	local data_index = ListBoxGetDataIndex ("EmojiiChooseIconDialogIcons", WindowGetId (SystemData.MouseOverWindow.name))
	local icon_index = tonumber (SystemData.ActiveWindow.name:match ("EmojiiChooseIconDialogIconsRow%d+Icon(%d+)"))		
	local IndexNumber = L"31"..wstring.sub( Emojii.chooseIconDialogListData[data_index][icon_index],3)

	Tooltips.CreateTextOnlyTooltip(SystemData.MouseOverWindow.name,nil)
	Tooltips.SetTooltipText( 1, 1, towstring(Emojii.Emotelist[tonumber(IndexNumber)]))
	Tooltips.SetTooltipColorDef( 1, 1, Tooltips.MAP_DESC_TEXT_COLOR )
	Tooltips.Finalize()    
	Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_TOP )
end

function Emojii.OnMouseOverStart2()
	local WinParent = WindowGetParent(SystemData.MouseOverWindow.name)
	local WindowName = towstring(SystemData.MouseOverWindow.name)

	Tooltips.CreateTextOnlyTooltip(SystemData.MouseOverWindow.name,nil)
	Tooltips.SetTooltipText( 1, 1, towstring(Emojii.Emotelist[Emojii.SelectedMoji]))
	Tooltips.SetTooltipText( 2, 1, L"LeftClick: Select Emojii")	
	Tooltips.SetTooltipText( 3, 1, L"RightClick: Post Emojii")		
	Tooltips.SetTooltipColorDef( 1, 1, Tooltips.MAP_DESC_TEXT_COLOR )
	Tooltips.Finalize()    
	Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_TOP )
end


